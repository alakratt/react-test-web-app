import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
                  <form>
            <div className='row center-align'>
<div className='col s12'>
        <div className="input-field col s6">
          <input id="first_name" type="text" className="validate" required></input>
          <label for="first_name">First Name</label>
        </div>

         <div className="input-field col s6">
          <input id="last_name" type="text" className="validate"></input>
          <label for="last_name">Last Name</label>
        </div>
        
     
      </div>
      </div>

      <div className='row center-align'>
<div className='col s12'>
        <div className="input-field col s6">
          <input id="pwd" type="password" className="validate" required></input>
          <label for="pwd">Password</label>
        </div>

         <div className="input-field col s6">
          <input id="confirmPwd" type="password" className="validate" required></input>
          <label for="confirmPwd">Confirm Password</label>
        </div>
      </div>
      </div>

        <div className='row center-align'>
<div className='col s12'>
        <div className="input-field col s6">
          <input id="email" type="email" className="validate" required></input>
          <label for="email">Email</label>
        </div>

        
      </div>
      </div>

<div className='row center-align'>
<div className='col s12'>
      <button className='waves-effect waves-light btn blue'>Register</button>
      </div>
      </div>
       </form>
    );
  }
}

export default App;
