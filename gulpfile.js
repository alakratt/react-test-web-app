const {css, js} = require('./frontend');
const {
	src,
	dest,
	symlink
} = require('gulp');
const inject = require('gulp-inject');
const es = require('event-stream');


let cssFn = () => {
	return src(css).pipe(dest(`public`));
};

let jsFn = () => {
	return src(js).pipe(dest(`public`));
}

const build = () => {
	let cssStream = cssFn();
	let jsStream = jsFn();

	return src('public/index.html').pipe(inject(es.merge(cssStream, jsStream), { relative: true, addRootSlash: true })).pipe(dest('public'));
}


exports.default = build;
