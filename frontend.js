const css = ['node_modules/materialize-css/dist/**/*.css',
		'!node_modules/materialize-css/dist/**/*.min.css'
	];

const js = ['node_modules/materialize-css/dist/**/*.js',
		'!node_modules/materialize-css/dist/**/*.min.js'
	];

module.exports = {css, js}